module.exports = function (plop) {

    plop.setGenerator('block', {
        description: 'Create a new Gutenberg Block Package',
        prompts: [
            {
                type: 'input',
                name: 'blockSlug',
                message: 'Block Slug (the block will render this partial)?',
            },
            {
                type: 'input',
                name: 'blockTitle',
                message: 'Block title?',
            },
            {
                type: 'input',
                name: 'blockDescription',
                message: 'Block description?',
                default: '',
            },
            {
                type: 'input',
                name: 'blockIcon',
                message: 'Block icon',
                default: 'dashicons-align-full-width',
            },
            {
                type: 'list',
                name: 'blockCategory',
                message: 'Block category?',
                choices: [
                    'common',
                    'formatting',
                    'layout',
                    'widgets',
                    'embed',
                ]
            },
        ],
        actions: function (data) {
            return [
                {
                    type: 'add',
                    path: `template-parts/gutenberg/${data.blockSlug}/block.json`,
                    templateFile: 'plop/plop-templates/new-block/block-json.hbs',
                    data: {
                        blockSlug: data.blockSlug,
                        blockFunctionName: `${data.blockSlug.replaceAll(
                            '-',
                            '_'
                        )}`,
                        blockTitle: data.blockTitle,
                        blockDescription: data.blockDescription,
                        blockIcon: data.blockIcon,
                        blockCategory: data.blockCategory,
                        blockSlugTrim: `${data.blockSlug.replaceAll(
                            '_',
                            '-'
                        )}`
                    },
                },
                {
                    type: 'add',
                    path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}-registration.php`,
                    templateFile: 'plop/plop-templates/new-block/block-registration.hbs',
                    data: {
                        blockSlug: data.blockSlug,
                        blockFunctionName: `${data.blockSlug.replaceAll(
                            '-',
                            '_'
                        )}`,
                        blockTitle: data.blockTitle,
                        blockDescription: data.blockDescription,
                        blockIcon: data.blockIcon,
                        blockCategory: data.blockCategory,
                        blockSlugTrim: `${data.blockSlug.replaceAll(
                            '_',
                            '-'
                        )}`
                    },
                },
                {
                    type: 'add',
                    path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}.css`,
                },
                {
                    type: 'add',
                    path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}.js`,
                },
                {
                    type: 'add',
                    path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}-admin.css`,
                },
                {
                    type: 'add',
                    path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}-admin.js`,
                },
                {
                    type: 'add',
                    path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}.php`,
                },
            ]
        },
    })
}