module.exports = function (plop) {
  const titleCase = plop.getHelper('titleCase')

  plop.setGenerator('block', {
    description: 'Create a BenchPress ACF Block Partial class',
    prompts: [
      {
        type: 'input',
        name: 'partialName',
        message: 'Partial name (the block will render this partial)?',
      },
      {
        type: 'input',
        name: 'blockTitle',
        message: 'Block title?',
        default: function (answers) {
          return titleCase(answers.partialName)
        },
      },
      // {
      //   type: 'input',
      //   name: 'className',
      //   message: 'Block Class name?',
      //   default: function(answers) {
      //     return `${titleCase(answers.partialName).replace(
      //       /[\s\/]/g,
      //       '_'
      //     )}_Block`
      //   },
      // },
      {
        type: 'input',
        name: 'blockDescription',
        message: 'Block description?',
        default: '',
      },
      {
        type: 'list',
        name: 'blockCategory',
        message: 'Block category?',
        choices: [
          'common',
          'formatting',
          'layout',
          'widgets',
          'embed',
          'other',
        ],
      },
      {
        type: 'input',
        name: 'blockCategory',
        message:
          'Custom category name (this category needs to be registered in order to work)?',
        when: function (answers) {
          return answers.blockCategory === 'other'
        },
      },
    ],
    actions: function (data) {
      return [
        {
          type: 'add',
          path: `template-parts/gutenberg/${data.partialName}/${data.partialName}.block.php`,
          templateFile: 'benchpress-block.hbs',
          data: {
            className: `${titleCase(data.partialName).replace(
              /[\s\/]/g,
              '_'
            )}_Block`,
          },
        },
      ]
    },
  })
}
