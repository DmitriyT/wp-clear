<?php
get_header();

// get_search_query();

if ( have_posts() ) :

			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/cards/search' );

			endwhile;

//			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif;

get_footer();
