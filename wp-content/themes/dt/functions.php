<?php
/**
 * Require Theme Setup Settings
 */
require_once ('theme-setup/theme-setup.php');

/**
 * Start Master Classes
 */
require_once ('classes/master/Hooks.php');
require_once ('classes/master/ACF.php');
require_once ('classes/master/CPT.php');
require_once ('classes/master/ENQUEUE_SETUP.php');
require_once ('classes/master/Gutenberg.php');

add_filter( 'jpeg_quality', function( $quality ){ return 100; } );
add_filter( 'big_image_size_threshold', '__return_false' );