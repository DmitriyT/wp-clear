var gulp = require('gulp');
/**
 * Plugins
 */
var sass = require('gulp-sass')(require('sass'));
var flatten = require('gulp-flatten');
const autoprefixer = require('gulp-autoprefixer');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
const babel = require('gulp-babel');
var jshint = require('gulp-jshint');
const minifyCSS = require('gulp-minify-css')
/**
 * Files Path
 */
var MAIN_SASS = "./assets/sass/main.sass";
var BLOCKS_SASS = "./assets/sass/blocks/*.sass";
var DEST_CSS = "./assets/css/";
var DEST_CSS_BLOCKS = "./assets/css/blocks";
var CSS = [
    "./assets/css/*.css",
    "./assets/css/**/*.css",
    "./assets/css/**/*/*.css"
];
/**
 * Tasks
 */
gulp.task('lint:js', function() {
    return gulp.src('assets/js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
        .pipe(jshint.reporter('fail'));
});

gulp.task('build-libs:css', function() {
    return gulp.src
    ([
        'assets/libs/*.css',
        'assets/libs/**/*.css',
        'assets/libs/**/*/*.css',
        'assets/libs/**/*/*/*.css',
    ])
        .pipe(concat('libs.css'))
        .pipe(minifyCSS())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('assets/css/'));
});

gulp.task('build-libs:js', function() {
    return gulp.src
    ([
        'node_modules/bootstrap/js/dist/*.js',
        'node_modules/bootstrap/js/dist/**/*.js',
        'assets/libs/*.js',
        'assets/libs/**/*.js',
        'assets/libs/**/*/*.js',
        'assets/libs/**/*/*/*.js',
    ])
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat('libs.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest('assets/js/'));
});

gulp.task('build:css', function () {
    return gulp.src(MAIN_SASS)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(DEST_CSS));
});

gulp.task('autoprefixer', function () {
    return gulp.src(CSS)
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 3 versions'],
            cascade: false
        }))
        .pipe(gulp.dest(DEST_CSS));
});

gulp.task('build-blocks:css', function () {
    return gulp.src(BLOCKS_SASS)
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(DEST_CSS_BLOCKS));
});