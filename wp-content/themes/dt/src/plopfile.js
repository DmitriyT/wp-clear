const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
const { exec } = require("child_process");

module.exports = function(plop) {

    /**
     * Trigger gulp commands for compilie CSS & JS files
     */
    plop.setActionType('buildThemeCssJs', function (answers, config, plop) {

        exec("gulp build-libs:css", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        });
        exec("gulp build-libs:js", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        });
        exec("gulp build:css", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        });
        exec("gulp autoprefixer", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        });
        exec("gulp build-blocks:css", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        });

    });

    plop.setGenerator('build', {
        prompts : [],
        actions : function (data){

            var actions = [];

            actions.push({
                type: 'buildThemeCssJs',
            });

            return actions;
        }
    });

}