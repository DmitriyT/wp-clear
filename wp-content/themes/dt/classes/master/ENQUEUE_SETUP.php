<?php

class ENQUEUE_SETUP {

	public function __construct() {
		add_action( 'wp_enqueue_scripts', [$this, 'setup_styles'] );
		add_action( 'wp_enqueue_scripts', [$this, 'setup_scripts'] );
	}

	public function setup_styles(){

		wp_enqueue_style( 'theme-style', get_stylesheet_uri(), array(), _S_VERSION );
		wp_style_add_data( TEXTDOMAIN . '-style', 'rtl', 'replace' );

		$css_files = scandir(get_template_directory() . '/assets/css/');

		if(!empty($css_files)){
			if(in_array('.', $css_files)){
				unset($css_files[array_search('.', $css_files)]);
			}
			if(in_array('..', $css_files)){
				unset($css_files[array_search('..', $css_files)]);
			}
			if(in_array('blocks', $css_files)){
				unset($css_files[array_search('blocks', $css_files)]);
			}

			$i = 1;
			foreach ( $css_files as $file ) {
				wp_enqueue_style(TEXTDOMAIN . '-style-' . $i, get_template_directory_uri() . '/assets/css/' . $file, [], _S_VERSION);
				$i++;
			}
		}

	}

	public function setup_scripts(){

		wp_enqueue_script("jquery");

		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		$js_files = scandir(get_template_directory() . '/assets/js/');

		if(!empty($js_files)){
			if(in_array('.', $js_files)){
				unset($js_files[array_search('.', $js_files)]);
			}
			if(in_array('..', $js_files)){
				unset($js_files[array_search('..', $js_files)]);
			}
			if(in_array('blocks', $js_files)){
				unset($js_files[array_search('blocks', $js_files)]);
			}

			$i = 1;
			foreach ( $js_files as $file ) {

				$in_footer = ($file === 'libs.min.js') ? false : true;

				wp_enqueue_script(TEXTDOMAIN . '-js-' . $i, get_template_directory_uri() . '/assets/js/' . $file, [], _S_VERSION, $in_footer);

				$i++;
			}
		}

	}

}

new ENQUEUE_SETUP();