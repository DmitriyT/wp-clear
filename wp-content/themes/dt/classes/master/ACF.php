<?php

class ACF_SETUP {

	public function __construct() {

		require get_template_directory() . '/classes/ACF/ACF_Admin_Settings.php';
		require get_template_directory() . '/classes/ACF/ACF_Options_Pages_Generate.php';
		require get_template_directory() . '/classes/ACF/ACF_CENTUM_Builder_Class.php';

		$this->create_option_pages();

		add_action('admin_head', [$this, 'some_styles']);
		add_filter('acf-flexible-content-preview.images_path', [$this, 'change_previews_path'], 10, 2);

	}

	/**
	 * Path to ACF content builder previews.
	 */
	function change_previews_path(){
		$path = 'lib/acf-flexible-content-preview';
		return $path;
	}

	/**
	 * Create ACF Options Pages
	 */
	public function create_option_pages(){

		$option_page = new ACF_Options_Pages_Generate();

		$option_page->add_option_page( 'Theme Settings', 'theme-settings' );

		$option_page->acf_parent_slug = 'theme-settings';
		$option_page->add_option_sub_page('Services', 'services');

	}

	/**
	 * Styles for ACF content builder previews.
	 */
	public function some_styles(){
		echo '<style>
            body .acf-fc-popup ul li a .acf-fc-popup-image{
            background-repeat: no-repeat!important;
            background-size: contain!important;
            }
        </style>';
	}

}

new ACF_SETUP();