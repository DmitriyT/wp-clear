<?php

class CPT_SETUP {

	public function __construct() {

		require get_template_directory() . '/classes/CPT/Class_CPT_Generate.php';
		require get_template_directory() . '/classes/CPT/Reviews_CPT.php';

	}

}

new CPT_SETUP();