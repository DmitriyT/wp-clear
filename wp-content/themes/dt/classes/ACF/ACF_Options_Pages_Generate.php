<?php
class ACF_Options_Pages_Generate {

	public $acf_parent_slug;

	public function add_option_page($page_name, $page_slug){

		if(function_exists('acf_add_options_page')){

			acf_add_options_page(array(
				'page_title' => $page_name,
				'menu_title' => $page_name,
				'menu_slug' => $page_slug,
				'post_id' => $page_slug,
				'capability' => 'edit_posts',
				'redirect' => false
			));

		}

	}

	public function add_option_sub_page($page_name, $page_slug){

		if(function_exists('acf_add_options_sub_page')){

			acf_add_options_sub_page(array(
				'page_title' => $page_name,
				'menu_title' => $page_name,
				'menu_slug' => $page_slug,
				'parent_slug'	=> $this->acf_parent_slug,
			));

		}

	}

}