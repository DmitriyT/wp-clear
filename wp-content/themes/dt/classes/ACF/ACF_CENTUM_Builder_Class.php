<?php

class ACF_CENTUM_Builder_Class {

	public $slug = 'content_builder';

	public function __construct($id = '', $slug = '') {

		if(empty($id)){
			$id = $this->setup_id();
		}

		if(!empty($slug) && is_string($slug)){
			$this->slug = $slug;
		}

		$this->render_builder($id);

	}

	public function setup_id(){
		$id = get_the_ID();

		if(is_home()){
			$id = get_option('page_for_posts');
		}

		if(is_archive()){
			$id = 'options';
			$this->setup_slug(get_post_type());
		}

		return $id;
	}

	private function setup_slug($post_type){
		$this->slug = 'archive_' . $post_type . '_' . $this->slug;
	}

	public function render_builder($id){
		if( have_rows($this->slug, $id) ){
			while( have_rows($this->slug, $id) ) : the_row();
				get_template_part('template-parts/builder/components/' . get_row_layout());
			endwhile;
		}
	}

}