<?php
class ACF_Admin_Settings {

	public function __construct() {

		add_filter( 'acf-flexible-content-preview.images_path', array( $this, 'change_previews_path' ), 10, 2 );
		add_action( 'admin_head', array( $this, 'change_previews_styles' ) );

	}

	/**
	 * Styles for ACF content builder previews.
	 */
	function change_previews_styles() {
		echo '<style>
		    body .acf-fc-popup ul li a .acf-fc-popup-image{
		    background-repeat: no-repeat!important;
		    background-size: contain!important;
		}
		  </style>';
	}

	/**
	 * Path to ACF content builder previews.
	 */
	function change_previews_path() {

		$path = 'lib/acf-flexible-content-preview';
		return $path;

	}

}

new ACF_Admin_Settings();