<?php
class Class_CPT_Generate {

	public $cpt_args = [
		'label'              => null,
		'labels'             => [
			'name'               => 'CPT',
			'singular_name'      => 'CPT',
			'add_new'            => 'Add',
			'add_new_item'       => 'Add',
			'edit_item'          => 'Edit',
			'new_item'           => 'New',
			'view_item'          => 'View',
			'search_items'       => 'Search',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in cart',
			'parent_item_colon'  => '',
			'menu_name'          => 'CPT',
		],
		'description'        => '',
		'public'             => true,
		'publicly_queryable' => true,
		'menu_position'      => null,
		'menu_icon'          => 'dashicons-format-aside',
		'capability_type'    => 'post',
		'hierarchical'       => false,
		'supports'           => [ 'title', 'page-attributes' ],
		'taxonomies'         => [],
		'has_archive'        => false,
		'rewrite'            => true,
		'query_var'          => true,
	];

	public $slug = 'slug';

	public function __construct( $slug = 'slug', $args = [] ) {

		if ( empty( $args ) || ! is_array( $args ) ) {

			return;

		}

		$this->slug = $slug;

		$this->setup_args_array( $args );

		add_action( 'init', [ $this, 'register_api_cpt' ] );
	}

	private function setup_args_array( $args ) {

		if ( ! empty( $args ) && is_array( $args ) ) {

			foreach ( $args as $key => $value ) {

				if ( $key == 'labels' ) {

					if ( ! empty( $value ) && is_array( $value ) ) {

						foreach ( $value as $k => $v ) {

							$this->cpt_args['labels'][ $k ] = $v;

						}

					}

				}

				$this->cpt_args[ $key ] = $value;

			}

		}

		return $args;

	}

	public function register_api_cpt() {
		register_post_type( $this->slug, $this->cpt_args );
	}

}