<?php

class Reviews_CPT {

	public function register_reviews_cpt() {
		$args = array(
			'labels'    => [
				'name'          => 'Reviews',
				'singular_name' => 'Review',
				'menu_name'     => 'Reviews',
			],
			'supports'  => [ 'title', 'page-attributes' ],
			'menu_icon' => 'dashicons-admin-comments',
			'rewrite'   => true,
			'public'    => false,
			'show_ui'   => true
		);

		new Class_CPT_Generate( 'reviews', $args );
	}

}

