<?php
if ( ! defined( 'TEXTDOMAIN' ) ) {

	define( 'TEXTDOMAIN', 'iceland' );

}
if ( ! defined( '_S_VERSION' ) ) {

	define( '_S_VERSION', '1.0.0' );

}