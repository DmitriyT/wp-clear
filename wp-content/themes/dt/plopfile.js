const path = require('path');
const fs = require('fs');
const fse = require('fs-extra');
const { exec } = require("child_process");

module.exports = function(plop) {

    /**
     * Create Folders Structure for Gutenberg
     */
    plop.setActionType('createFoldersStructure', function (answers, config, plop) {

        if (!fs.existsSync(config.dir)){
            fs.mkdirSync(config.dir);
        }

    });

    /**
     * Create JS & CSS files for Gutenberg structure
     */
    plop.setActionType('createCssJsStructure', function (answers, config, plop) {

        if (fs.existsSync(config.dir)){

            var cssFile = './src/assets/css/blocks/' + config.fileSlug + '.css';
            var jsFile = './src/assets/js/blocks/' + config.fileSlug + '.js';

            if(fs.existsSync(cssFile)){
                fs.copyFile(cssFile, config.dir + '/' + config.fileSlug + '.css', function (){});
                fs.createWriteStream(config.dir + '/' + config.fileSlug + '-admin.css');
            }

            if(fs.existsSync(jsFile)){
                fs.copyFile(jsFile, config.dir + '/' + config.fileSlug + '.js', function (){});
                fs.createWriteStream(config.dir + '/' + config.fileSlug + '-admin.js');
            }


        }

    });

    /**
     * Create Gutenberg Render Files
     */

    plop.setActionType('CreateGutenbergRenderFiles', function (answers, config, plop) {

        if (fs.existsSync(config.dir) && fs.existsSync(config.filePath)){

            fs.writeFile(
                config.dir + '/' + config.fileSlug + '.php',
                fs.readFileSync(config.filePath, 'utf-8'),
                function (){});

        }

    });

    /**
     * Migrate fonts from src to WP theme
     * TODO: ADD folder exist check
     */
    plop.setActionType('migrateFonts', function(answers, config, plop){

        const fonts_srcDir = './src/assets/fonts/';
        const fonts_destDir = './assets/fonts/';

        try {
            fse.copySync(fonts_srcDir, fonts_destDir, { overwrite: true|false })
            console.log('success!')
        } catch (err) {
            console.error(err)
        }

    });

    /**
     * Migrate images from src to WP theme
     * TODO: ADD folder exist check
     */
    plop.setActionType('migrateImg', function(answers, config, plop){

        const img_srcDir = './src/assets/img/';
        const img_destDir = './assets/img/';

        try {
            fse.copySync(img_srcDir, img_destDir, { overwrite: true|false })
            console.log('success!')
        } catch (err) {
            console.error(err)
        }

    });

    /**
     * Migrate CSS from src to WP theme
     * TODO: ADD folder exist check
     */
    plop.setActionType('migrateCss', function(answers, config, plop){

        const css_srcDir = './src/assets/css/';
        const css_destDir = './assets/css/';

        try {
            fse.copySync(css_srcDir, css_destDir, { overwrite: true|false })
            console.log('success!')
        } catch (err) {
            console.error(err)
        }

    });

    /**
     * Migrate CSS from src to WP theme
     * TODO: ADD folder exist check
     */
    plop.setActionType('migrateJs', function(answers, config, plop){

        const js_srcDir = './src/assets/js/';
        const js_destDir = './assets/js/';

        try {
            fse.copySync(js_srcDir, js_destDir, { overwrite: true|false })
            console.log('success!')
        } catch (err) {
            console.error(err)
        }

    });

    /**
     * Migrate SASS Files from src to WP theme
     * TODO: ADD folder exist check
     */
    plop.setActionType('migrateSass', function(answers, config, plop){

        var sass_srcDir = './src/assets/sass/';
        var sass_destDir = './assets/sass/';

        try {
            fse.copySync(sass_srcDir, sass_destDir, { overwrite: true|false })
            console.log('success!')
        } catch (err) {
            console.error(err)
        }

    });

    /**
     * Migrate CSS Libs
     * TODO: ADD folder exist check
     */
    plop.setActionType('migrateLibs', function(answers, config, plop){

        var libs_srcDir = './src/assets/libs/';
        var libs_destDir = './assets/libs/';

        try {
            fse.copySync(libs_srcDir, libs_destDir, { overwrite: true|false })
            console.log('success!')
        } catch (err) {
            console.error(err)
        }

    });

    plop.setActionType('migrate', function (answers, config, plop) {

        exec("plop migrate:blocks", (error, stdout, stderr) => {});
        exec("plop migrate:assets", (error, stdout, stderr) => {});

    });

    plop.setGenerator('migrate', {
        prompts : [],
        actions : function (data){

            var actions = [];

            actions.push({
                type: 'migrate',
            });

            return actions;
        }
    });

    plop.setGenerator('migrate:assets', {
        prompts : [],
        actions : function (data){

            var actions = [];

            actions.push({
                type: 'migrateCss',
            });

            actions.push({
                type: 'migrateSass',
            });

            actions.push({
                type: 'migrateJs',
            });

            actions.push({
                type: 'migrateLibs',
            });

            actions.push({
                type: 'migrateJpg',
            });

            actions.push({
                type: 'migrateFonts',
            });

            return actions;

        }
    });

    plop.setGenerator('migrate:block', {
        prompts: [
            {
                type: 'input',
                name: 'fileSlug',
                message: 'Block Slug (the block will render this partial)?',
                validate: function (value) {

                    if (fs.existsSync('./src/html/blocks/' + value + '.html')) {
                        return true;
                    }

                    return 'File ' + value + '.html not Found by path ./src/html/blocks/';

                },
            }
        ],
        actions: function (data){

            var fileSlug = data.fileSlug;
            var dir = './template-parts/gutenberg/' + fileSlug;
            var filePath = './src/html/blocks/' + fileSlug + '.html';
            var actions = [];

            actions.push({
                type: 'createFoldersStructure',
                fileSlug: fileSlug,
                dir: dir
            });

            actions.push({
                type: 'createCssJsStructure',
                fileSlug: fileSlug,
                dir: dir
            });

            actions.push({
                type: 'CreateGutenbergRenderFiles',
                fileSlug: fileSlug,
                filePath: filePath,
                dir: dir
            });

            actions.push({
                type: 'add',
                path: `template-parts/gutenberg/${fileSlug}/block.json`,
                templateFile: 'plop/plop-templates/new-block/block-json.hbs',
                data: {
                    blockSlug: fileSlug,
                    blockFunctionName: `${fileSlug.replaceAll(
                        '-',
                        '_'
                    )}`,
                    blockTitle: makeTitle(fileSlug),
                    blockDescription: '',
                    blockIcon: 'dashicons-cover-image',
                    blockCategory: 'common',
                    blockSlugTrim: `${fileSlug.replaceAll(
                        '_',
                        '-'
                    )}`
                },
            });

            actions.push({
                type: 'add',
                path: `template-parts/gutenberg/${fileSlug}/${fileSlug}-registration.php`,
                templateFile: 'plop/plop-templates/new-block/block-registration.hbs',
                data: {
                    blockSlug: fileSlug,
                },
            });

            return actions;

        }
    })

    plop.setGenerator('migrate:blocks', {
        prompts: [],
        actions: function (data){

            const files = fs.readdirSync('./src/html/blocks');
            actions = [];

            files.map(file => {

                var fileSlug = path.basename(file).replace('.html', '');
                var dir = './template-parts/gutenberg/' + fileSlug;
                var filePath = './src/html/blocks/' + file;

                actions.push({
                    type: 'createFoldersStructure',
                    fileSlug: fileSlug,
                    dir: dir
                });

                actions.push({
                    type: 'createCssJsStructure',
                    fileSlug: fileSlug,
                    dir: dir
                });

                actions.push({
                    type: 'CreateGutenbergRenderFiles',
                    fileSlug: fileSlug,
                    filePath: filePath,
                    dir: dir
                });

                actions.push({
                    type: 'add',
                    path: `template-parts/gutenberg/${fileSlug}/block.json`,
                    templateFile: 'plop/plop-templates/new-block/block-json.hbs',
                    data: {
                        blockSlug: fileSlug,
                        blockFunctionName: `${fileSlug.replaceAll(
                            '-',
                            '_'
                        )}`,
                        blockTitle: makeTitle(fileSlug),
                        blockDescription: '',
                        blockIcon: 'dashicons-cover-image',
                        blockCategory: 'common',
                        blockSlugTrim: `${fileSlug.replaceAll(
                            '_',
                            '-'
                        )}`
                    },
                });

                actions.push({
                    type: 'add',
                    path: `template-parts/gutenberg/${fileSlug}/${fileSlug}-registration.php`,
                    templateFile: 'plop/plop-templates/new-block/block-registration.hbs',
                    data: {
                        blockSlug: fileSlug,
                    },
                });

            })

            return actions;

        }
    });

    plop.setGenerator('new:block', {
            description: 'Create a new Gutenberg Block Package',
            prompts: [
                {
                    type: 'input',
                    name: 'blockSlug',
                    message: 'Block Slug (the block will render this partial)?',
                },
                {
                    type: 'input',
                    name: 'blockTitle',
                    message: 'Block title?',
                },
                {
                    type: 'input',
                    name: 'blockDescription',
                    message: 'Block description?',
                    default: '',
                },
                {
                    type: 'input',
                    name: 'blockIcon',
                    message: 'Block icon',
                    default: 'dashicons-cover-image',
                },
                {
                    type: 'list',
                    name: 'blockCategory',
                    message: 'Block category?',
                    choices: [
                        'common',
                        'formatting',
                        'layout',
                        'widgets',
                        'embed',
                    ]
                },
            ],
            actions: function (data) {
                return [
                    {
                        type: 'add',
                        path: `template-parts/gutenberg/${data.blockSlug}/block.json`,
                        templateFile: 'plop/plop-templates/new-block/block-json.hbs',
                        data: {
                            blockSlug: data.blockSlug,
                            blockFunctionName: `${data.blockSlug.replaceAll(
                                '-',
                                '_'
                            )}`,
                            blockTitle: data.blockTitle,
                            blockDescription: data.blockDescription,
                            blockIcon: data.blockIcon,
                            blockCategory: data.blockCategory,
                            blockSlugTrim: `${data.blockSlug.replaceAll(
                                '_',
                                '-'
                            )}`
                        },
                    },
                    {
                        type: 'add',
                        path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}-registration.php`,
                        templateFile: 'plop/plop-templates/new-block/block-registration.hbs',
                        data: {
                            blockSlug: data.blockSlug,
                            blockFunctionName: `${data.blockSlug.replaceAll(
                                '-',
                                '_'
                            )}`,
                            blockTitle: data.blockTitle,
                            blockDescription: data.blockDescription,
                            blockIcon: data.blockIcon,
                            blockCategory: data.blockCategory,
                            blockSlugTrim: `${data.blockSlug.replaceAll(
                                '_',
                                '-'
                            )}`
                        },
                    },
                    {
                        type: 'add',
                        path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}.css`,
                    },
                    {
                        type: 'add',
                        path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}.js`,
                    },
                    {
                        type: 'add',
                        path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}-admin.css`,
                    },
                    {
                        type: 'add',
                        path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}-admin.js`,
                    },
                    {
                        type: 'add',
                        path: `template-parts/gutenberg/${data.blockSlug}/${data.blockSlug}.php`,
                    },
                ]
            },
        })

}

function makeTitle(slug) {
    var words = slug.split('-');

    for (var i = 0; i < words.length; i++) {
        var word = words[i];
        words[i] = word.charAt(0).toUpperCase() + word.slice(1);
    }

    return words.join(' ');
}
