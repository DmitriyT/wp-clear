<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package DT
 */

get_header();

    while ( have_posts() ) :
    	the_post();

    	get_template_part( 'template-parts/single/' . get_post_type() );

    endwhile;

get_footer();