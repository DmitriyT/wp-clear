<?php
add_action( 'theme_post_navigation', 'render_theme_post_navigation' );

function render_theme_post_navigation() {
	the_post_navigation(
		array(
			'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'dt' ) . '</span> <span class="nav-title">%title</span>',
			'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'dt' ) . '</span> <span class="nav-title">%title</span>',
		)
	);
}