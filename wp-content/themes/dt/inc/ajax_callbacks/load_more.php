<?php

class Load_More {

	protected static $instance;

	private function __construct() {

		if ( wp_doing_ajax() ) {
			add_action( 'wp_ajax_load_more', array( $this, 'load_more_function' ) );
			add_action( 'wp_ajax_nopriv_load_more', array( $this, 'load_more_function' ) );
		}

	}

	public function load_more_function(){

		if ( ! wp_verify_nonce( $_POST['nonce'], 'crypto-load-more-nonce' ) ) {
			die( 'Stop!' );
		}

		$page = (isset($_POST['page']) && !empty($_POST['page'])) ? intval($_POST['page']) : '';

		if(empty($page)){
			return false;
		}

		$args                = json_decode( stripslashes( $_POST['query'] ), true );
		$args['paged'] = (int)$page + 1;

		$query = new WP_Query( $args );

		$html = '';
		ob_start();

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) {
				$query->the_post();

				get_template_part( 'template-parts/cards/' . get_post_type() );

			}
		}

		wp_reset_postdata();

		$html = ob_get_contents();
		ob_end_clean();

		$responce = array(
			'html'  => $html,
			'query' => $query,
			'page' => $args['paged'],
			'max_pages' => $query->max_num_pages,
			'found_posts' => $query->found_posts
		);

		echo json_encode( $responce );
		die();

	}

	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self();
		}

		return self::$instance;
	}

}

Load_More::get_instance();