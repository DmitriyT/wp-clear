<?php
function pvd( $var ) {
	echo "<pre style='font-size:16px;'>";
	var_dump( $var );
	echo "</pre>";
}

function round_to_tens($num, $need_tens = false){
	if(empty($num)) return;

	if (preg_match('/\.\d{1,}/', round($num, 2)) || $need_tens != false) {
		return number_format($num, 2, '.', '');
	} else {
		return $num;
	}

}

/**
 * @param $link
 *
 * @return string|void
 */
function getLinkAttributes( $link ) {
	if ( empty( $link ) && ! is_array( $link ) ) {
		return;
	}

	if ( str_replace( $_SERVER['HTTP_HOST'], '', $link['url'] ) ) {
		return 'href="' . esc_url( $link['url'] ) . '" target="' . esc_attr( $link['target'] ) . '" title="' . esc_attr( $link['title'] ) . '"';
	} else {
		return 'href="' . esc_url( $link['url'] ) . '" rel="nofollow" target="' . esc_attr( $link['target'] ) . '" title="' . esc_attr( $link['title'] ) . '"';
	}

}

/**
 * @param $image_id
 * @param $width
 * @param int $height
 * @param false $crop
 * @param bool $aq_single
 * @param false $aq_upscale
 *
 * @return string
 */
function getImageAttributesById( $image_id, $width = 0, $height = 0 ) {

	$image_alt = get_post_meta( $image_id, '_wp_attachment_image_alt', true );
	$image_alt = ( ! empty( $image_alt ) ) ? $image_alt : get_the_title( $image_id );

	$image_title = wp_get_attachment_metadata( $image_id );
	$image_title = ( ! empty( $image_title['image_meta']['title'] ) ) ? $image_title['image_meta']['title'] : $image_alt;

	$image_url = wp_get_attachment_image_url( $image_id, 'full' );

	$attrs = [
		'title' => esc_attr( $image_title ),
		'alt'   => esc_attr( $image_alt ),
		'src'   => esc_url( $image_url )
	];

	if ( ! empty( $width ) && $width != 0 ) {
		$attrs['width'] = (int) $width;
	}

	if ( ! empty( $height ) && $height != 0 ) {
		$attrs['height'] = (int) $height;
	}


	$image_html = ' ';
	foreach ( $attrs as $key => $value ) {

		$image_html .= $key . '="' . $value . '" ';

	}

//	return 'title="' . esc_attr( $image_title ) . '" alt="' . esc_attr( $image_alt ) . '" src="' . esc_url($image_url) . '"';
	return $image_html;
}

function inline_svg( $file, $is_fromlibrary = null ) {
	if ( $is_fromlibrary ) {
		echo file_get_contents( $file );
	} else {
		echo file_get_contents( get_template_directory() . '/assets/img/svg/' . $file );
	}
}