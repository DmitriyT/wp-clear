<?php
if ( ! is_search() ) {
	the_content();
}

if(class_exists('ACF_CENTUM_Builder_Class')){
	new ACF_CENTUM_Builder_Class();
}