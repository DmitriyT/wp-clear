<?php
define( 'WP_CACHE', false ); // Added by WP Rocket

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'clear' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', 'root' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'asu`]#JiFU$[/ JWl*m+hj@ml^6ifWQAudgP>N2|1Rx KMmuQ-zijQ}XCu _~hs>' );
define( 'SECURE_AUTH_KEY',  'nH @=1y%L_.v0jz!q#}5s5FOE[@0HcjXqsTIqD/+EObL)zl*zSv]@u4LE7UPpd1K' );
define( 'LOGGED_IN_KEY',    'p#K)5`i(`V p]_l<d|rDBHz8ZQJJ V=#] x}jX1AX2J0A6s%i5{^0i9pidM_u}l=' );
define( 'NONCE_KEY',        'Z/d/H`$AW Ed2Fq.p~!t{XmT-[4(qB{F#jX>0K:5B4Xbkz-VN_QcWT n~G~=u]w]' );
define( 'AUTH_SALT',        '^Uoh^.3&Jy|HktFr81MNN/]=J yuf1Nt8WK;zYk]0Z.ybte<tpM-fnn$%r^E,Zoz' );
define( 'SECURE_AUTH_SALT', '2lnO-dD+2He}NqgyK9~T[=cW#=jEBCWQ>2hgH1/OK]$ik)[{_bbE|8.X_[C$-j:@' );
define( 'LOGGED_IN_SALT',   '*xTuow}%+:M>9w>-TGa B]#<1=-8KQuf;-mC_[H7c=JCIj|%Jt*,B#d]:7{|L=5N' );
define( 'NONCE_SALT',       ' A8yL`Wr3Z5,uQ3yeKf8,<Y/s`n)Y),rdH{b{Mx*W)K#tOKEaV0U4UqB6}v+iqq9' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'dt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
define( 'WP_DEBUG_LOG', true );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
